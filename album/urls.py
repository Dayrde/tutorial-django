from django.urls import path
from album import views

app_name='album'
urlpatterns = [
    path('', views.first_view, name='first_view'),
    path('category/', views.category, name='category'),
    path('category/<int:category_id>/detail/', views.category_details, name='category_detail'),
    path('photo/', views.PhotoListView.asView(), name='photo_list'),
    path('photo/<int:photo_id>/detail', views.PhotoDetailView.asView(), name='photo_detail')
]